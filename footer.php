<footer>
  <div class="container">
    <div class="row">
        <div class="column-5">
          <?php the_field('main_site_links','option'); ?>
        </div>
        <div class="column-5">
          <?php the_field('resource_centre','option'); ?>
        </div>
        <div class="column-5">
          <?php the_field('contact','option'); ?>
        </div>
        <div class="column-5">
          <?php the_field('follow_us','option'); ?>
        </div>
        <div class="column-5">
          <?php the_field('logo_description','option'); ?>
        </div>
    </div>
  </div>
</footer>

<div class="bluefooter">
  <div class="container">
      <div class="row">
          <div class="column-2">
          <?php the_field('copyright','option'); ?>
      </div>
          <div class="column-2">
          <?php the_field('privacy_policy','option'); ?>
    </div>
  </div>
</div>

<script>
  function myFunction() {
    var x = document.getElementById("lrssbnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
</script>

<?php wp_footer(); ?>
