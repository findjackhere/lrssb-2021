<?php get_header(); /* Template Name: Homepage */ ?>

<div class="container">


<div class="row">
    <div class="column-1">
    <h1 style="text-align:center;">News</h1>
    </div>
</div>

<div class="row">
    <div class="column-1">
        <p style="display:inline-block;">Filter:</p>
        <a href="/category/news/" style="display:inline-block;"><button class="primary-button"> News</button></a>
        <a href="/category/newsletters/" style="display:inline-block;"><button class="primary-button"> Newsletters</button></a>
    </div>
</div>
    

<div class="row">

            <?php $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' => 20,
        'order'          => 'DESC',
        'paged'          => $paged)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
            <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
            <ul>
                <a href="<?php the_permalink(); ?>">
                
                    <div class="column-3">
                            <li class="card">
                                <div class="thumbnailimage">  
                                    <?php the_post_thumbnail(); ?> 
                                </div>
                                <div class="lowercard">
                                    <h3 style="margin:0;">
                                        <?php the_title(); ?>
                                    </h3>
                                        <?php the_excerpt(); ?>
                                        <button class="primary-button">View</button>
                                </div>  
                            </li>
                        </div>
                </a>
            </ul>
    
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php else : ?>
            <?php endif; ?>

</div>

</div>


<?php wp_footer(); ?>
<?php get_footer(); ?>
