<?php get_header(); /* Template Name: Board */ ?>

<div class="row">
    <div class="column-3"><br></div>
        <div class="column-3">
            <div class="meetourteamheader">
                <?php the_field('header'); ?>
            </div>
        </div>
    <div class="column-3"><br></div>
</div>


<div class="container">

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

    <div class="row">
        <div class="column-4">
            <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/suebyrne.png" style="width:100%";>
                <div class="teamlowercard">   
                    <h2 style="margin-bottom:10px;">Sue Byrne</h2>
                    <h4 style="margin-top:0;">Chair</h4>
                    <p>With nearly 30 years in the logistics industry, where she has held a number of high-profile roles, Sue brings a wealth of experience to her role at the LRSSB.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>
        </div>
        <div class="column-4">
        <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/mikemabey.png" style="width:100%";>
                <div class="teamlowercard">   
                    <h2 style="margin-bottom:10px;">Mike Mabey</h2>
                    <h4 style="margin-top:0;">Board Member</h4>
                    <p>A senior manager with 29 years’ experience of customer service and operations within the light rail environment, Mike is currently Head of Operations for Keolis Nottingham.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>
        </div>
        <div class="column-4">
        <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/bobmorris.png" style="width:100%";>
                <div class="teamlowercard">   
                    <h2 style="margin-bottom:10px;">Bob Morris</h2>
                    <h4 style="margin-top:0;">Board Member</h4>
                    <p>em ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>        
        </div>
        <div class="column-4">
        <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/jonfox.png" style="width:100%";>
                <div class="teamlowercard">   
                    <h2 style="margin-bottom:10px;">Jon Fox</h2>
                    <h4 style="margin-top:0;">Board Member</h4>
                    <p>em ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>        
        </div>
     </div>

    <?php endwhile; ?>
<?php endif; ?>
 
</div>


<?php wp_footer(); ?>
<?php get_footer(); ?>
