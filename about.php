<?php get_header(); /* Template Name: About */ ?>

<div class="homepageheader">
    <h1 class="headerdisplaytext">About LRSSB<h1>
</div>

<div class="container">

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

    <div class="row">
        <div class="column-1">
             <?php the_field('aboutintro'); ?>
        </div>
    </div>

    <?php endwhile; ?>
<?php endif; ?>
 
</div>

<div class="bluecontainer">
    <div class="container">
        <div class="row">
            <div class="column-1">
                <?php the_field('becomeamember'); ?>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
<?php get_footer(); ?>
