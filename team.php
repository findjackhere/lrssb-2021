<?php get_header(); /* Template Name: Team */ ?>

<div class="row">
    <div class="column-3"><br></div>
        <div class="column-3">
            <div class="meetourteamheader">
                <?php the_field('header'); ?>
            </div>
        </div>
    <div class="column-3"><br></div>
</div>


<div class="container">

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

    <div class="row">
        <div class="column-3">
        <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/carlwilliams.png" style="width:100%";>
                <div class="teamlowercard">   
                    <h2 style="margin-bottom:10px;">Carl Williams</h2>
                    <h4 style="margin-top:0;">Chief Executive</h4>
                    <p>For more than 25 years, Carl Williams has worked in public transport operations and maintenance and joined the LRSSB in 2020 from West Midlands Metro.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>
        </div>
        <div class="column-3">
        <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/erica.png" style="width:100%";>
                <div class="teamlowercard">   
                    <h2 style="margin-bottom:10px;">Erica Pearson</h2>
                    <h4 style="margin-top:0;">Administration Manager</h4>
                    <p>Erica joined the LRSSB in April 2019 as an Administration Manager, having previously worked for West Midlands Metro as a Network Co-Ordinator.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>
        </div>
        <div class="column-3">
        <div class="teamcard">
                <img src="/wp-content/uploads/2021/03/mark.png" style="width:100%";>
                <div class="teamlowercard">    
                    <h2 style="margin-bottom:10px;">Mark Ashmore</h2>
                    <h4 style="margin-top:0;">Safety Assurance Manager</h4>
                    <p>em ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    <a href="mailto:carl.williams@tfwm.org.uk"><button class="primary-button">Email</button></a>
                </div>
            </div>        
        </div>
     </div>

    <?php endwhile; ?>
<?php endif; ?>
 
</div>


<?php wp_footer(); ?>
<?php get_footer(); ?>
