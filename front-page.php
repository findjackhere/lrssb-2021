<?php get_header(); /* Template Name: Homepage */ ?>



<div class="homepageheader">
    <h1 class="headerdisplaytext">Welcome to LRSSB<h1>
</div>

<div class="homepageaboutbox center">
    <?php the_field('homepageaboutbox'); ?>
</div>

<div class="container">

<div class="row">
    <div class="column-1">
        <h2>Latest News</h2>
    </div>
</div>

<div class="row">

<?php $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>3)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

<ul>
    <a href="<?php the_permalink(); ?>">
        <div class="column-3">
                <li class="card">
                    <div class="thumbnailimage">  
                        <?php the_post_thumbnail(); ?> 
                    </div>
                    <div class="lowercard">
                        <h3 style="margin:0;">
                            <?php the_title(); ?>
                        </h3>
                            <?php the_excerpt(); ?>
                            <button class="primary-button">View</button>
                    </div>  
                </li>
            </div>
    </a>
</ul>

<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>
    
</div>

<div class="row" style="margin-top:-20px;">
    <div class="column-1">
        <h2>Resource Centre</h2>
    </div>
</div>


<div class="row">
        <div class="column-2">
            <div class="allresourcescard">
                <?php the_field('resourcecentreleft'); ?>
            </div>
    </div>
    <div class="column-2">
        <div class="logincard">
            <?php the_field('resourcecentreright'); ?>
        </div>
    </div>
</div>

</div>

<div class="bluecontainer">
    <div class="container">
        <div class="row">
            <div class="column-1">
                <?php the_field('becomeamember'); ?>
            </div>
        </div>
    </div>
</div>

 

<?php wp_footer(); ?>
<?php get_footer(); ?>
