<?php get_header(); /* Template Name: Contact */ ?>

<div class="row">
    <div class="column-3"><br></div>
        <div class="column-3">
            <div class="meetourteamheader">
                <?php the_field('header'); ?>
            </div>
        </div>
    <div class="column-3"><br></div>
</div>


<div class="container">

<div class="row">
    <div class="column-2">
    <?php the_field('contact_copy'); ?>
    </div>
        <div class="column-2">
                <?php the_field('map'); ?>
        </div>
</div>

 
</div>


<?php wp_footer(); ?>
<?php get_footer(); ?>
