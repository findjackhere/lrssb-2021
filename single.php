<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
  
get_header(); ?>
  
<div class="container">
  <div class="row">
    <div class="column-1">

    <?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

    <h2><?php the_title(); ?></h2>

    <?php endwhile; ?>
    <?php endif; ?>

     </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="column-1">

    <?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

  <img src="<?php the_field('header_image'); ?>" alt="Article Header Image" class="newsheaderimage" style="margin-bottom:30px;"/>

    <?php endwhile; ?>
    <?php endif; ?>

    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="column-news-left">

    <?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

        <?php the_content(); ?>

    </div>

    <div class="column-news-right">
    <h3>Information</h3>
    <?php the_date(); ?>
    <h3>Recent News</h3>
    <?php $recent_posts = wp_get_recent_posts();
		  foreach( $recent_posts as $recent ){
		  echo '<li>
		  <a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';   }
		  wp_reset_query(); ?>


    </div>

    <?php endwhile; ?>
    <?php endif; ?>

   </div>
</div>



<?php wp_footer(); ?>
<?php get_footer(); ?>