<?php get_header(); /* Template Name: News */ ?>
 
<div class="container">

<a href="/news/"><button class="primary-button" style="margin-top:20px;margin-bottom:5px;">Back to all News</button></a>

<div class="row">

<?php $wpb_all_query = new WP_Query(array('post_type'=>'post', 'category_name' => 'news', 'post_status'=>'publish', 'posts_per_page' => 20,
'order'          => 'DESC',
'paged'          => $paged)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
<ul>
    <a href="<?php the_permalink(); ?>">
    
        <div class="column-3">
                <li class="card">
                    <div class="thumbnailimage">  
                        <?php the_post_thumbnail(); ?> 
                    </div>
                    <div class="lowercard">
                        <h3 style="margin:0;">
                            <?php the_title(); ?>
                        </h3>
                            <?php the_excerpt(); ?>
                            <button class="primary-button">View</button>
                    </div>  
                </li>
            </div>
    </a>
</ul>

<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>


</div>

</div>

</div>


<?php get_footer(); ?>
